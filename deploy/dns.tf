# create data reference within Terraform used to retrieve aws_route53_zone
data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}
# add records to the zone to give us subdomains
resource "aws_route53_record" "app" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}" # we return the subdoamin api.stage adn route name api.stage/dev.test.net
  type    = "CNAME"                                                                            # canonical name links it to another existing dns name
  ttl     = "300"

  records = [aws_lb.api.dns_name]
}
# create a certificate
resource "aws_acm_certificate" "cert" {
  domain_name       = aws_route53_record.app.fqdn # this is the full domain name for our env fqdn fully qualified domain name
  validation_method = "DNS"

  tags = local.common_tags

  # our terraform running smooth when we are destroying our env
  lifecycle {
    create_before_destroy = true
  }
}
# its pupose is for domain validation, we are creating a new record
resource "aws_route53_record" "cert_validation" {
  name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.zone.zone_id
  records = [
    aws_acm_certificate.cert.domain_validation_options.0.resource_record_value # a random string generated when we created the certificate
  ]
  ttl = "60"
}
# used to start the validation process for our dns record
resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn # certificate theat we want to validate
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}