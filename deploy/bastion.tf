// aws_ami is the image to be used to  create the virtual machine
// amazon linux ami is the amazon linux distribution the most optimized 
// return the id of the latest image
// add a wildcard amzn2-ami-kernel-5.10-hvm-2.0.20230404.1-x86_64-gp2 so we can return the most recent ones
data "aws_ami" "amazon_linux" { // data is for retrieving things from aws // the second name can be custom amazon_linux https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"] // we get official amazon images, we don't want to  be getting any image that other peopel have maybe uploaded
}

# it will create a new iam role with the policy to assume the role
resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

# attach the policy to iam role
resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly" # this is a policy predefined in AWS
}

# create the iam instance profile for our bsation server
resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

// create resource
resource "aws_instance" "bastion" {
  ami                  = data.aws_ami.amazon_linux.id // retrieve the id of the ami
  instance_type        = "t2.micro"                   // how much resourcs will be assigned to that machine https://aws.amazon.com/ec2/instance-types/
  user_data            = file("./templates/bastion/user-data.sh")
  iam_instance_profile = aws_iam_instance_profile.bastion.name
  key_name             = var.bastion_key_name
  subnet_id            = aws_subnet.public_a.id # we want the bastion instance to be accesible on the public internet

  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]

  // to add additional tag we can use this feature merge
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  tags = local.common_tags
}