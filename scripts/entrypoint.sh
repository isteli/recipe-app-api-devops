#!/bin/sh

set -e

# serve the static files through proxy
python manage.py collectstatic --noinput
# run migrations
python manage.py wait_for_db
python manage.py migrate
# run uswgi on socket port 9000, we set 4 workers on our container, master flag run this as a master service on the terminal
# the module is the application that uWSGI is going to run (app/uswgi.py)
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
