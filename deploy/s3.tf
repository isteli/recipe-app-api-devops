# create a new bucket
resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-2023-05-18-files" # name
  #acl           = "public-read"           # access control list
  force_destroy = true # it does not allow you to destory without confirming it, it allows us with our terraform to destory it 
}