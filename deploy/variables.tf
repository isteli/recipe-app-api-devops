// every resource will have attached this prefix
variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "stelianivan29@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for hte RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

# if we want to deploy from our local machine we can use this default value => the value is already under CI/CD/piplienes/variables
variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "542882798562.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image proxy latest"
  default     = "542882798562.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

# we'll add this to our sample vars
variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "test.net" # the domain name registered
}

# subdomain used to split out our domain name for our different accounts
variable "subdomain" {
  description = "Subdomain per env"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}