# add multiple subnets to your database
resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id,
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

# add security group that allows you to control inbound and outbound access allowed to the resource on port 5432 default port for postgres
resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id # assign the vpc for our security group

  # controls what the rules are for hte inbound access
  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    security_groups = [
      # allow access to bastion
      aws_security_group.bastion.id,
      # to get access from our db to our ECS service
      aws_security_group.ecs_service.id,
    ]
  }

  tags = local.common_tags
}

resource "aws_db_instance" "main" {
  identifier              = "${local.prefix}-db"
  name                    = "recipe" # name of the db inside our postgres instance
  allocated_storage       = 20       # space 20gb 
  storage_type            = "gp2"    # entry level storage type
  engine                  = "postgres"
  engine_version          = "11.15"
  instance_class          = "db.t2.micro" # type of db server we want to run
  db_subnet_group_name    = aws_db_subnet_group.main.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0     # set the number of days you want to maintain backup 
  multi_az                = false # determines if the db should be running on multiple availability zones
  skip_final_snapshot     = true  # when you destroy the db it will create the final snapshot of the data that was in that db 
  vpc_security_group_ids  = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}