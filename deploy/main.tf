terraform {
  backend "s3" {
    bucket         = "learning-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate" # define the key in which the state file is going to be stored inside the bucket
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "learning-recipe-app-api-devops-tfstate-lock" # define the dinamoDb table so you can't run the same terraform on two different places, on the same infrastructure in the same tim, avoids any conflicts issues
  }
}

# we need to define the provider (AWS, GCP, Azure digitalization)
# you need to lock the version to prevent changes from newer version to propagate and could broke your code
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}" // is terraform syntax called interpolation syntax
  common_tags = {
    Environmnet = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# it gives us a resource to retrive the current region that we are working on
data "aws_region" "current" {} 