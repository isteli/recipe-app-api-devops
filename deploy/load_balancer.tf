resource "aws_lb" "api" {
  name               = "${local.prefix}-main"
  load_balancer_type = "application" # two main types: application or network balancer
  # to be accessible from the internet
  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [aws_security_group.lb.id] # it will be defined later

  tags = local.common_tags
}

# we add all our ecs tasks to our target group
resource "aws_lb_target_group" "api" {
  name        = "${local.prefix}-api"
  protocol    = "HTTP"          # our load balancer is going to send request to our api using http protocol
  vpc_id      = aws_vpc.main.id # we make sure the load balancer is in the same vpc as our app
  target_type = "ip"
  port        = 8000 # port oru proxiy will run on in ECS task

  # it does a periodic check of our app and it determines if the app is healty or not, if is not then it tells ECS to restart our app and try it ot make it healthy
  health_check {
    path = "/admin/login/"
  }
}

# accepts the requests to our load blanacer and send the requests to the target gorup
resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "api_https" {
  load_balancer_arn = aws_lb.api.arn
  port              = 443
  protocol          = "HTTPS"

  # we want to make sure that the validation has been initiated for the certificate
  # it creates the validation object before it creates our listener
  certificate_arn = aws_acm_certificate_validation.cert.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}

# security group that allows access to our load balancer
resource "aws_security_group" "lb" {
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id

  # accepts all connections from the public internet in to our load blanacer
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  # access that load balancer has into our application we can forward the requets to our app
  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}