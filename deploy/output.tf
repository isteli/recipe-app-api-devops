# outpout of our db address
output "db_host" {
  value = aws_db_instance.main.address
}
# it will output the bastion host, we'll know the address to connect to
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_enpoint" {
  value = aws_route53_record.app.fqdn
}