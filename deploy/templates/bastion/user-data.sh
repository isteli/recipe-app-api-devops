#!/bin/bash

# you are using aws_ami image
sudo yum update -y # udpate with the lastes packages
sudo amazon-linux-extras install -y docker # install docker
sudo systemctl enable docker.service  # enalbe docker
sudo systemctl start docker.service # start docker on the bastion system
sudo usermod -aG docker ec2-user # add our ec2-user to docker group, the default user is calles ec2-user because we are using aws_ami
